phpstan-baseline:
	php -d memory_limit=-1 ./vendor/bin/phpstan analyse --memory-limit=-1 --generate-baseline=./phpstan-baseline.neon --allow-empty-baseline
